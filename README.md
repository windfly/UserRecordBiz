# 疫情防控人员进出登录-JAVA后端

#### 介绍
疫情期间，社区无纸化登记信息，依靠微信，自主扫描快速记录信息。与[《疫情防控人员进出登录-前端》](https://gitee.com/windfly/UserRecord-uniapp)是一套

#### 技术
后台逻辑使用自己的开源框架 [DON ](https://gitee.com/windfly/DON)+ 开源工具包 [windfly](https://gitee.com/windfly/windfly-util) + JPA 配合实现，使用mysql数据库


#### 运行
配置好数据库链接和文件参数后，直接运行 UserRecord_Jetty 的main 方法即可  
![1.png](1.png)  
数据库配置，classes/META-INF 目录下的 persistence.xml 文件  
classes目录下 config.properties 文件中的微信公众号APPID和appsecret，以及微信oauth的回调路径  
注意：使用DON框架，要用标准目录结构，也就是，eclipse项目class文件输出要输出到 WEB-INF/classes 文件夹中